####serveur X.org#####

1) Présentation

X.org-----famille serveur X

X.org----serveur graphique

seul----usage limité --->utilisationn gestionnaire de fenetres / environnement de bureau (kde,gnome,xfce....)

2) Architecture

X.org ----client/serveur

*le serveur X est lancé sur une machine possédant un écran,un clavier et une souris

* le client X se connecte au serveur afin de lui donner des requêtes (applications graphiques affichées dans une fenetre du serveur X)

* protocole X ---permet l'échange entre le client et le serveur


3) installation

package (debian/ redhat & cie): xorg

xorg :"metapackage"

*xf86-video-vesa: pilote graphique générique

*xorg-fonts-100(75)dpi: police de caractères

*xorg-res-utils: pour gérer les ressources de xorg

*xorg-server: serveur graphique

*xorg-server-utils: utilitaires indispensables au serveur

*xorg-twm: gestionnaire de fenetre par defaut de x.org

*xorg-utils: utilitaires pour avoir des infos sur le serveur

*xorg-init: scripts d'initialisation de session

*xterm: émulateur de terminal par défaut de xorg



configuration

/etc/X11
conf---->/usr/share/X11/xorg.conf.d

section ---->bloc informaations sur un périphérique

exemple: Section "Screen"

Identifier "Screen0"
Device     "Screen0"
Monitor    "Screen0"
DefautDepth  24


Section "Device"

    Identifier "Device0"
    Driver "ati"
    VendorName "ATI"


*ecran (screen)  -----carte graphique:Device + un moniteur (monitor)

*clavier & souris   -----inputDevice

screen+ inputDevice  -------ServerLayout


3) scripts de configuration

*pilotes

lspci [ grep VGA]

nvidia----nv/nvidia

ati------ati,radeon(libre)/ catalyst (propriétaire)

intel-----intel(libre), i810-i910


**configuration/reconfiguration

dpkg-reconfigure xorg

$ X -configure (root)

tester la configuration:
ctrl+alt+backspace  pour killer le serveur X
$X -config /root/xorg.conf.new

Si OK ----déplacer la conf dans le répertoire approprié

$ xorgconfig

Cas particuliers (drivers propriétaires)

$ nvidia-xconfig

$ aticonfig --initial -f


4) A connaitre

* "couche d'abstration materielle" (HAL "Hardware Abstraction Layer")----assurer la communication entre le matériel et les applications

à lancer au boot avec les autres daemons

* startx
lancé depuis une console  ------> ~/.xinitrc

#! /bin/sh

# ~/.xinitrc

# exec gnome-session
# exec startkde
# exec xfce4

*accélération graphique

accélération activée? 

$ glxinfo | grep "direct rendering"


****************************************************************************************************************************************

Accessibilité

1) système complet

Adriane: linux complet avec les outils d'accessibilité activés par défaut

2) Affichage

*Kmag(KDE):loupe

*Compiz:ensemble outils (grossisseur,filtre de couleurs)

*DAMP (Debian Accessibility Magnification Packages)

3) Lecture écran

*Orca (gnome,mate,unity)

*YaSR: lecteur écran en mode console (nécessite un moteur de synthèse vocale)

4) Synthèse vocale

Kmouth (KDE)

5) Pilotage de la souris

Enable viacam: permet de piloter la souris grâce à la webcam

6) Daltonisme

color-blind-applet
compiz: fonctionnalité de filtrage de couleur

