#!/bin/bash
set -e

#on cherche une adresse ip dans le fichier /etc/resolv.conf
grep -q 192.168.1.20 /etc/resolv.conf

#Avez-vous trouvé l'adresse ip? Utilisation des codes de sortie
if [ $? -eq 0]
	then
		echo "Succes: vous avez trouvé l'adresse ip"
	exit 0
	else
		echo "Echec: l'adresse ip n'a pas ete trouve, echec du script" >$2
	exit 1
fi
