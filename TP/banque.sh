#!/usr/bin/bash
echo "- Ex. 2: Banque"

declare -i balance=$((RANDOM % 1000)) \
    credit=-500 \
    withdrawl=

printf "Your balance: %i\nMax. credit: %i\n" $balance $credit

read -p "Withdrawl? " withdrawl

if [[ $((balance - withdrawl)) -lt $credit ]]; then 
    echo "DENIED" 
    exit 1
fi
