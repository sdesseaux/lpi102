BEGIN {
  # Tester le nombre d'arguments
  if (ARGC != 3) {
    printf("Usage : awk -f error_log.awk type_message fichier_log\n") | "cat 1>&2"
    exit 1
  }

  typeMessage = ARGV[1] 
  chaineMessage = "\\[" ARGV[1] "\\]"
  delete ARGV[1]
  print "LISTE DES MESSAGES DE TYPE : " , typeMessage 

}

$0 ~ chaineMessage {
  #  Utiliser le | comme separateur de champ
  sub(/^\[/,"") 
  sub(/] \[/,"|") 
  sub(/] /,"|") ;
  split($0,ligne,"|")
   
  print "Date : " ligne[1]
  print ligne[3]
}
