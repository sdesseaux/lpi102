#!/bin/bash

# Modification du séparateur de champ IFS (Internal field separator
IFS=":"

#Creation boucle while + read avec en entree /etc/passwd
#et qui positionne les champs (separateur :) dans des variable
while read nom password ident reste
do
  # test 
  #if [[  ${ident} -ge 1000  && ${ident} -lt 3000 ]]
  # test arithmetique
  if (( ${ident} >= 1000 && ${ident} < 3000 ))
  then
    echo "${nom}"
  fi 
done < /etc/passwd
