#!/bin/bash


case $1 in
  start)
    echo "Starting"
    ;;
  stop) 
    echo "Stopping" 
    ;;
  status|state|--state)
    echo "Status:"
    ;;
  [1-9])
    echo "Nombre ${1}"
    ;;
  *)
    echo " Invalid option"
    exit 4
    ;;
esac
