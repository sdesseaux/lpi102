#!/bin/bash
#set -x

# ignorer CTRL+C et les CTRL+Z en utilisant trap
#trap 'echo "Vous ne pouvez pas stopper"' SIGINT

trap 'echo "signal INT recu"; touch /tmp/menu.err; exit 1' INT


function read_input(){
read -p "Entrez votre choix [ 1 - 4 ] " reponse
case $reponse in
    1)
      id 
      pause
      ;;
    2)
      echo $HOME
      pause
      ;;
    3)
      echo $(who | wc -l) " personnes connectées"
      pause
      ;;
    4)
      echo "Bye"
      exit 0
      ;;
    *)
      echo "Mauvais choix"
      pause
esac
}

function show_menu(){
date
echo "1- Votre id"
echo "2- Votre répertoire de tavail"
echo "3- Nombre de personnes connectées"
echo "4- Sortie"
}

function pause(){
message="Press [enter] key to continue..."
read -p "$message" readEnterKey
}

# main
while true
do
clear
show_menu
read_input
pause
done
