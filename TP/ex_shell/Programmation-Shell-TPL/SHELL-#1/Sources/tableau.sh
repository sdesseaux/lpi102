#!/bin/bash

# Déclaration d'un tableau à indice : clé numérique
declare -a tab1

# Déclaration d'un tableau associatif : choix nom de clé
declare -A tab2

# Ajout de valeur dans tab1
tab1+=(Lux Fr En It Es)

# Parcourir les éléments d'un tableau à indices
for i in ${tab1[@]}
do
  echo "Valeur : $i"
done
