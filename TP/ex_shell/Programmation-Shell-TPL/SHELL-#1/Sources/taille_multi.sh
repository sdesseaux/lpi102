#!/bin/bash
#set -x
# Script qui renvoi la taille d'un ou plusieurs fichiers passés en paramètre

# Test si nombre de paramètre bon
if [[ ${#} -lt 1 ]]
then
  echo "Erreur de lancement du script ${0}"
  echo "Usage : ${0} fichier [fichiern]"
  exit 1
fi 

for param in ${@}
do
  if [[ ! -f ${param} ]]
  then 
    echo "${param} n'est pas un fichier"
    exit 1
  fi

  size=$(ls -l ${param} | cut -d' ' -f5)

  echo "La taille du fichier ${param} est : ${size}ko"
done
