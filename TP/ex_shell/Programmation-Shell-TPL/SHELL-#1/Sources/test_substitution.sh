#!/bin/bash

echo ${var:- "La variable n'est pas definie"}
echo "1 -Value of var is ${var}"

echo ${var:="La variable n'est pas définie"}
echo "2 - Value of var is ${var}"

unset var
echo ${var:+"Voici la valeur par defaut"}
echo "3- Value of var is ${var}"
