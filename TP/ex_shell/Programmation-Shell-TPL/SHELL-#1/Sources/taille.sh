#!/bin/bash
set -x
# Script qui renvoi la taille du fichier passé en paramètre

# Test si nombre de paramètre bon
if [[ ${#} -lt 1 || ${#} -gt 1 ]]
then
  echo "Erreur de lancement du script ${0}"
  echo "Usage : ${0} fichier"
  exit 1
elif [[ ! -f ${1} ]]
then
  echo "${1} n'est pas un fichier"
  exit 1
fi

size=$(ls -l ${1} | cut -d' ' -f5)

echo "La taille du fichier ${1} est : ${size}ko"
