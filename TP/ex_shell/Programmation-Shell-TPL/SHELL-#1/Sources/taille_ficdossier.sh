#!/bin/bash
#set -x
# Script qui renvoi la taille d'un ou plusieurs fichiers passés en paramètre

# creation variable VERBOSE
VERBOSE=true

# Recuperation du nom du script dans une variable
JOBNAME=${0}

log() {
 # Fonction log qui envoie a message dans syslog et qui affiche dans la stdout
 # si mode verbose active
 local MESSAGE="${@}"
 # test si mode verbose à true
 if [[ "${VERBOSE}" = 'true' ]]
 then
   echo "${MESSAGE}"
 fi
 # Appel fonction looger pour envoyer le message dans syslog 
 # (/var/log/message pour redhat)
 logger -t ${JOBNAME} "${MESSAGE}"
} 
 
# Test si nombre de paramètre bon
if [[ ${#} -ne 1 ]]
then
  echo "Erreur de lancement du script ${0}"
  echo "Usage : ${0} repertoire"
  exit 1
# Test si le paramètre 1 n'est pas un répertoire
elif [[ ! -d ${1} ]]
then
  log "Erreur : ${1} n'est pas un répertoire"
  exit 1
fi 

# Creation d'un tableau
declare -a tableau

# Alimentation du tableau par la commande ls -1 ${1}
tableau+=($(ls -1 ${1}))

# Affiche tous les élément du tableau 
echo "${tableau[@]}"

log "Il y a ${#tableau[@]} éléments dans le repertoire ${1}"

for param in ${tableau[*]}
do
  if [[ ! -f ${1}/${param} ]]
  then 
    log "${1}/${param} n'est pas un fichier"
  else
    size=$(ls -l ${1}/${param} | cut -d' ' -f5)
    echo "La taille du fichier ${param} est : ${size}ko"
  fi
done
