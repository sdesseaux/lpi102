#!/bin/bash

# Script qui affiche différentes variables

# Assigner une variable
WORD='script'

# Affichage de la variable
echo "$WORD"

# Affichage variable + texte
echo "$WORD écrit pour la formation"

# coller du texte à la variable
echo "$WORDing"

# coller du texte à la variable : {}
echo "${WORD}ing"

WORD2='ed'

# Combinaison de variables
echo "${WORD}${WORD2}"

# Reaffection de valeur à une variable existante 
WORD2='end'
echo "${WORD2}"

# Affichage d'une variable d'environnement
echo "Votre UID est : ${UID}"

# Substitution de commande : affichage nom
username=$(id -un)
echo "Votre username est : ${username}"



