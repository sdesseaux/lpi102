#!/bin/bash
set -x
# Script de creation de user
# Appel de read pour saisie clavier

# Test sur l'UID utilisateur
if [[ "${UID}" -ne 0 ]]
then
  echo "VOus n'êtes pas root : ${UID}"
  echo "Ce script doit être lancé en root"
  exit 1
fi

# Demande username
read -p 'Entrez le nom du user à créer:' username

# Demande de commentaire
read -p 'Entrez un commentaire pour cet utilisateur:' comment

useradd -c "${comment}" -m ${username}

if [[ "$?" -ne 0 ]]
then
  echo "Pb à la création du user ${username}"
  exit 2
fi

# Generation du mot de passe aléatoire
password=$(date +%s%N)

echo "${username}:${password}" | chpasswd
if [[ "$?" -ne 0 ]]
then
  echo "Pb de creation du mot de passe : ${username}"
  exit 2
fi


# Forcer la modification du password à la premiere connexion"
passwd -e ${username}
if [[ "$?" -ne 0 ]]
then
  echo "Pb de parametrage passwd pour ${username}"
  exit 2
fi

# Affichage des informations
echo "username : $username"
echo "${password}"
