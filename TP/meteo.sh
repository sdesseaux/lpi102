#!/usr/bin/bash

echo "- Ex. 1: Météo"

declare msg=

while [[ -z "$msg" ]]; do

    read -p "Message (brouillard/pluie/chaleur)?"

    case "$REPLY" in
        brouillard) msg="attention brouillard! gardez vos distances";;
        pluie) msg="attention, risque d'aquaplanning";;
        chaleur) msg="fortes chaleurs, pensez à vous hydrater";;
        *) echo "Invalid message!"
    esac
done

